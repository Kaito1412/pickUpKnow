function AlumnosController($scope){
  $scope.formVisibility = false;

	$scope.alumnos = [
		{nombre:"Juan", telefono: "1234567890", curso:"Segundo ESO"},
		{nombre:"Rosa", telefono: "0987654321", curso:"Primero ESO"},
		{nombre:"Alberto", telefono: "1122334455", curso:"Segundo ESO"},
		{nombre:"Ana", telefono: "6677889900", curso:"Tercero ESO"}
	];
  $scope.Save = function(){
    $scope.alumnos.push({nombre: $scope.nuevoAlumno.nombre, 
                        telefono: $scope.nuevoAlumno.telefono,
                        curso: $scope.nuevoAlumno.curso})

      $scope.formVisibility = false;
    }

    $scope.ShowForm = function() {
      $scope.formVisibility = true;
    }
}
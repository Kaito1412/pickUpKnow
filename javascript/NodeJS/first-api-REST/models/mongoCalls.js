exports = module.exports = function(app, mongoose) {

	var callsSchema = new mongoose.Schema({
		idCall: {type: String},
		origin: {type: Number},
		toCall: {type: Number},
		timeCall: {type: String},
		duration: {type: String},
	});

	mongoose.model('calls', callsSchema);

};

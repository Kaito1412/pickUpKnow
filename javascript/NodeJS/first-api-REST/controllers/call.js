//File: controllers/call.js
var mongoose = require('mongoose');
var mongoCalls  = mongoose.model('calls');

//GET - Return all tvshows in the DB
exports.findAllCalls = function(req, res) {
	mongoCalls.find(function(err, calls) {
        if(err) res.send(500, err.message);

        console.log('GET /calls')
		res.status(200).jsonp(calls);
	});
};

//GET - Return a TVShow with specified ID
exports.findById = function(req, res) {
	mongoCalls.findById(req.params.id, function(err, calls) {
    if(err) return res.send(500, err.message);

    console.log('GET /calls/' + req.params.id);
		res.status(200).jsonp(calls);
	});
};

//POST - Insert a new TVShow in the DB
exports.addCall = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var calls = new mongoCalls({
		idCall: req.body.idCall,
		origin: req.body.origin,
		toCall: req.body.toCall,
		timeCall: req.body.time,
		duration: req.body.duration
	});

	calls.save(function(err, call) {
		if(err) return res.send(500, err.message);
        res.status(200).jsonp(call);
	});
};

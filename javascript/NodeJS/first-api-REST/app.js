var express = require("express");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var mongoose = require('mongoose');

var app = express();

// Connection to DB
mongoose.connect('mongodb://localhost/calls', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Import Models and controllers
var model = require('./models/mongoCalls')(app, mongoose);
var callsCtl = require('./controllers/call');

// Index Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("Hello world!");
});

app.use(router);

// API routes
var routeCalls = express.Router();

routeCalls.route('/calls')
  .get(callsCtl.findAllCalls)
  .post(callsCtl.addCall);

routeCalls.route('/call/:id')
  .get(callsCtl.findById);

app.use('/api', routeCalls);

// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});

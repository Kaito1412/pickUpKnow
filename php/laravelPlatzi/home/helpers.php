<?php
function view($template, $vars = array()) {
    extract($vars);
    require "views/$template.php";
}

function controller($name) {
    $file = "controllers/$name.php";

    if (file_exists($file)) {
        require $file;
    }
    else {
        header("HTTP/1.1 404 Not Found");
        exit("Pagina no encontrada");
    }
}
?>

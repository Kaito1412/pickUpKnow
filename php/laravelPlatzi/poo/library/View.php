<?php

class View extends Response {

    protected $template;
    protected $vars = array();

    public function __construct($template, $vars = array()) {

        $this->template = $template;
        $this->vars = $vars;
    }
    
    public function getVars() {
        return $this->vars;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function execute() {
    
        $template = 'views/' . $this->getTemplate() . '.php';
        $vars = $this->getVars();

        if (!file_exists($template)) {
            exit("No existe $template");
        }

        call_user_func(function () use ($template, $vars) {
            extract($vars);

            ob_start();

            require $template;

            $tpl_content = ob_get_clean();

            require 'views/layout.php';
            die($tpl_content);
        });

    }
}

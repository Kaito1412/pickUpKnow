# -*- coding: utf-8 -*-

from django import forms


class formContact(forms.Form):
    reason = forms.CharField(max_length=100)
    email = forms.EmailField(required=False, label='you email')
    message = forms.CharField(widget=forms.Textarea)

    def clean_message(self):
        message = self.cleaned_data['message']
        num_words = len(message.split())
        if num_words < 4:
            raise forms.ValidationError("¡Se requieren mínimo 4 palabras!")
        return message
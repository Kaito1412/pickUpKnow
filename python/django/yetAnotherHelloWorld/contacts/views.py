# -*- coding: utf-8 -*-
from django.shortcuts import render
from contacts.forms import formContact
from django.core.mail import send_mail
from django.http import HttpResponseRedirect


def contacts(request):
    if request.method == 'POST':
        form = formContact(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            '''send_mail(
                cd['asunto'],
                cd['mensaje'],
                cd.get('email', 'noreply@example.com'),
                    ['siteowner@example.com'],
             )'''
            return HttpResponseRedirect('/now/')
    else:
        form = formContact(initial={'reason': 'Your web is cool!!'})

    return render(request, 'contactForm.html', {'form': form})

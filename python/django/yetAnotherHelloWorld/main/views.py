# -*- coding: utf-8 -*-
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.shortcuts import render_to_response
from library.models import Book
import datetime


def index(request):
    return HttpResponse('hello world')


def debug(request):
    valor = request.META.items()
    valor.sort()
    html = []
    for k, v in valor:
        html.append('<tr><td><strong>%s</strong></td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))


def dateNow(request, template):
    itsNow = datetime.datetime.now()
    return render(request, 'now.html', {'now': itsNow})


def list_library(request, template):
    list_books = Book.objects.order_by('name')
    return render_to_response('now.html', {'now': list_books})


def book(request, year):
    try:
        offset = int(year)
    except ValueError:
        raise Http404()
    html = """<html><body><h1>El anyo es: %s</h1>
        </body></html>""" % (offset)
    return HttpResponse(html)
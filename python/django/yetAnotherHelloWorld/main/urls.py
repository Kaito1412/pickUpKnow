from django.conf.urls import include, url
from django.views.static import serve
from django.contrib import admin
from main import views as main
from library import views as library
from contacts import views as contacts
from django.conf import settings

urlpatterns = [
    url(r'^$', main.index),
    url(r'^restful/$', library.book),
    url(r'^now/$', main.dateNow, {'template': 'now.html'}),
    url(r'^book/(?P<year>\d{4})/$', main.book),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/$', library.search),
    url(r'^contacts/$', contacts.contacts),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT,
        }),
    ]
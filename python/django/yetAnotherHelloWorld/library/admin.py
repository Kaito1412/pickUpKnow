# -*- coding: utf-8 -*-

from django.contrib import admin
from library.models import Editor, Author, Book


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastName', 'email')
    search_fields = ('name', 'lastName')


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'editor', 'date_published')
    list_filter = ('date_published',)
    date_hierarchy = 'date_published'
    ordering = ('-date_published',)
    fields = ('title', 'authors', 'editor', 'date_published')
    filter_horizontal = ('authors',)
    raw_id_fields = ('editor',)

admin.site.register(Editor)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)

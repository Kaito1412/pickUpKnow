# -*- coding: utf-8 -*-
from django.db import models


class Editor(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=60)
    state = models.CharField(max_length=30)
    country = models.CharField(max_length=50)
    website = models.URLField(blank=True)

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "Editors"

    def __unicode__(self):
        return self.name


class Author(models.Model):
    name = models.CharField(max_length=30)
    lastName = models.CharField(max_length=40)
    email = models.EmailField(blank=True, verbose_name='e-­mail')

    class Meta:
        ordering = ["lastName"]
        verbose_name_plural = "Authors"

    def __unicode__(self):
        return '%s %s' % (self.name, self.lastName)


class Book(models.Model):
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    editor = models.ForeignKey(Editor)
    date_published = models.DateField(blank=True, null=True)
    cover = models.ImageField(upload_to='covers')

    class Meta:
        ordering = ["title"]
        verbose_name_plural = "Books"

    def __unicode__(self):
        return self.title

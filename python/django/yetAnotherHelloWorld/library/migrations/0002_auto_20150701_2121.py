# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='author',
            options={'ordering': ['lastName'], 'verbose_name_plural': 'Authors'},
        ),
        migrations.AlterModelOptions(
            name='book',
            options={'ordering': ['title'], 'verbose_name_plural': 'Books'},
        ),
        migrations.AlterModelOptions(
            name='editor',
            options={'ordering': ['name'], 'verbose_name_plural': 'Editors'},
        ),
        migrations.AlterField(
            model_name='author',
            name='email',
            field=models.EmailField(max_length=254, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='date_published',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='editor',
            name='website',
            field=models.URLField(blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from django.shortcuts import render
from library.models import Book
from django.http import HttpResponse, Http404


def list_object(request, model):
    listObjects = model.objects.all()
    template = 'library/%s_lista.html' % model.__name__.lower()
    return render(request, template, {'listObject': listObjects})


def search(request):
    errors = []
    if 'query' in request.GET:
        query = request.GET['query']
        if not query:
            errors.append('Por favor introduce un término de búsqueda.')
        elif len(query) > 20:
            errors.append('''Por favor introduce un término de búsqueda menor
            a 20 caracteres.''')
        else:
            books = Book.objects.filter(title__icontains=query)
            return render(request, 'result.html',
            {'books': books, 'query': query})

    return render(request, 'searchForm.html', {'errors': errors})


def book(request):
    def GET(request):
        return HttpResponse("GET response")

    def POST(request):
        return HttpResponse("POST response")

    if request.method == 'GET':
        return GET(request)
    elif request.method == 'POST':
        return POST(request)
    raise Http404